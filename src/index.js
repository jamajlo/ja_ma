import './index.scss';

import 'component/Page/Page.scss';
import 'component/Header/Header.scss';
import 'component/Button/Button.scss';
import 'component/Icon/Icon.scss';
import 'component/Overview/Overview.scss';
import 'component/Menu/Menu.scss';
import 'component/Avatar/Avatar.scss';
import 'component/Review/Review.scss';
import 'component/Rating/Rating.scss';
import 'component/Content/Content.scss';
import 'component/Profile/Profile.scss';
import 'component/Form/Form.scss';
import 'component/Modal/Modal.scss';
