const autoprefixer = require( 'autoprefixer' ),
	cssmqpacker = require( 'css-mqpacker' );

module.exports = {
	plugins: [
		autoprefixer,
		cssmqpacker( {
			sort: true
		} )
	]
}