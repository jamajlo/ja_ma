const path = require('path'),
	dirPath = path.resolve(__dirname, 'src'),
	distPath = path.resolve(__dirname, 'dist'),
	imagePath = path.resolve(__dirname, 'src/img'),
	nodeModulesPath = path.resolve(__dirname, 'node_modules'),
	webpack = require('webpack'),
	importOnce = require('node-sass-import-once'),
	CleanWebpackPlugin = require('clean-webpack-plugin'),
	HtmlWebpackPlugin = require('html-webpack-plugin'),
	MiniCssExtractPlugin = require('mini-css-extract-plugin');

const style = (mode, loaders) => {
	if (mode !== 'production') {
		return ['style-loader'].concat(loaders);
	}

	return [MiniCssExtractPlugin.loader].concat(loaders);
};

const image = mode => {
	const loaders = [
		{
			loader: 'file-loader',
			options: {
				limit: 10000,
				name: 'img/[name].[ext]',
				publicPath: '/'
			}
		}
	];

	if (mode !== 'production') {
		return loaders.concat('image-webpack-loader');
	}

	return loaders.concat({
		loader: 'image-webpack-loader',
		options: {
			bypassOnDebug: true,
			mozjpeg: {
				progressive: true
			},
			gifsicle: {
				optimizationLevel: 7,
				interlaced: false
			},
			optipng: {
				optimizationLevel: 7
			},
			pngquant: {
				quality: '70',
				speed: 3
			}
		}
	});
};

const plugins = mode => {
	const plugins = [
		new HtmlWebpackPlugin({
			template: './src/index.html'
		})
	];

	if (mode !== 'production') {
		return plugins;
	}

	return plugins.concat([
		new webpack.DefinePlugin({
			'process.env.NODE_ENV': JSON.stringify(mode)
		}),
		new CleanWebpackPlugin('dist'),
		new MiniCssExtractPlugin({
			filename: 'css/[name].[contenthash].css',
			publicPath: '../'
		})
	]);
};

const config = (env, argv) => {
	const mode = argv.mode;

	return {
		entry: {
			index: './src/index.js'
		},

		output: {
			path: distPath,
			filename: 'js/[name].[chunkhash].js'
		},

		optimization: {
			splitChunks: {
				cacheGroups: {
					vendor: {
						chunks: 'initial',
						test: nodeModulesPath,
						name: 'vendor',
						enforce: true
					},
					styles: {
						name: 'styles',
						test: /\.s?css$/,
						minChunks: 1
					}
				}
			}
		},

		resolve: {
			modules: ['node_modules', 'src'],
			alias: {
				img: imagePath
			}
		},

		module: {
			rules: [
				{
					test: /\.js$/,
					include: dirPath,
					use: 'strict-loader'
				},
				{
					test: /\.css$/,
					use: style(mode, ['css-loader'])
				},
				{
					test: /\.scss$/,
					use: style(mode, [
						'css-loader',
						'resolve-url-loader',
						'postcss-loader',
						{
							loader: 'sass-loader',
							options: {
								importer: importOnce,
								includePaths: [dirPath, nodeModulesPath]
							}
						}
					])
				},
				{
					test: /\.html$/i,
					use: {
						loader: 'html-loader',
						options: {
							interpolate: 'require'
						}
					}
				},
				{
					test: /\.(jpe?g|png|gif|cur)(\?[a-z0-9]+)?$/i,
					exclude: path.join(dirPath, 'font'),
					use: image(mode)
				},
				{
					test: /\.(eot|svg|ttf|woff|woff2)(\?[a-z0-9=&.-]+)?$/i,
					exclude: imagePath,
					use: {
						loader: 'file-loader',
						options: {
							name: 'font/[name].[ext]'
						}
					}
				}
			]
		},

		plugins: plugins(mode)
	};
};

module.exports = config;
